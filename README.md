# vivaldi-ffmpeg-codecs

Additional support for proprietary codecs for vivaldi

https://ffmpeg.org/

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/browsers/vivaldi-ffmpeg-codecs.git
```

